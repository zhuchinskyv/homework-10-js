"use strict"
/** константи обєкти
 * @const
 * @property
 */
const SIZE_SMALL = { name: 'small', price: 50, kcal: 20 };
const SIZE_LARGE = { name: 'large', price: 100, kcal: 40 };
const STUFFING_CHEESE = { name: 'cheese', price: 10, kcal: 20 };
const STUFFING_SALAD = { name: 'salad', price: 20, kcal: 5 };
const STUFFING_POTATO = { name: 'potato', price: 15, kcal: 10 };
const TOPPING_MAYO = { name: 'mayo', price: 20, kcal: 5 };
const TOPPING_SPICE = { name: 'spice', price: 15, kcal: 0 };
/**
*Класс, обєкти якого описують параметри гамбургера. 
* @constructor
* @param size  розмір
* @param stuffing начинка
*/
let Hamburger = class Hamburger {

    constructor(size, stuffing) {
        this._size = size;
        this._stuffing = stuffing;
        this._allTopping = [];
    }
    /**
    * додаємо добавку до гамбургеру. Можна додати декілька добавок, якщо вони різні
    * @param topping тип добавки
    * @throws {HamburgerException}  при неправильному використанні
    */
    addTopping(topping) {
        for (let elem of Object.values(this._allTopping)) {
            try {
                if (elem.name === topping.name) {
                    throw new Error('duplicate topping')
                }
            }
            catch (e) {
                console.log(e.message)
            }
        }
        if (topping === TOPPING_MAYO || TOPPING_SPICE) {
            this._allTopping.push(topping)
        }
        try {
            if (!topping) {
                throw new Error('no topping given')
            }
        }
        catch (e) {
            console.log(e.message)
        }
    }
    /**
    * відмінити добавку, при умові, що вона раніше була додана  
    * @param topping   тип добавки
    * @throws {HamburgerException}  при неправильному використанні
    */
    removeTopping(topping) {
        if (topping === TOPPING_MAYO || topping === TOPPING_SPICE) {
            this._allTopping = this._allTopping.filter(elem => elem.name != topping.name)
        }
    }
    /**
    * отримати список добавок.
    * @return {Array} массив доданих добавок, утримує константи TOPPING_*
    */
    getTopping() {
        return this._allTopping;
    }
    /**
    * размір гамбургера
    */
    getSize() {
        return this._size;
    }

    setSize(size) {
        if (size === SIZE_LARGE || size === SIZE_SMALL) {
            this._size = size;
        }
    }
    /**
    * начинкa гамбургера
    */
    getStuffing() {
        return this._stuffing;
    }

    setStuffin(stuffing) {
        if (stuffing === STUFFING_CHEESE || stuffing === STUFFING_SALAD || stuffing === STUFFING_POTATO) {
            this._stuffing = stuffing;
        }
    }
    /**
    * цінa гамбургера
    * @return {Number} ціна
    */
    calculatePrice() {
        let sumPrice = 0;
        for (let elem of Object.values(this)) {
            if (elem.price) {
                sumPrice += +elem.price;
            }
        }
        for (let elem of Object.values(this._allTopping)) {
            try {
                if (elem.price) {
                    sumPrice += +elem.price;
                } else {
                    throw new Error('no topping given')
                }
            }
            catch (e) {
                console.log(e.message)
            }
        }
        return sumPrice;
    }
    /**
    * калорійність
    * @return {Number} калорійність в Ккалоріях
    */
    calculateCalories() {
        let sumKcal = 0;
        for (let elem of Object.values(this)) {
            if (elem.kcal) {
                sumKcal += +elem.kcal;
            }
        }
        for (let elem of Object.values(this._allTopping)) {
            try {
                if (elem.kcal) {
                    sumKcal += +elem.kcal;
                } else {
                    throw new Error('no topping given')
                }
            }
            catch (e) {
                console.log(e.message)
            }
        }
        return sumKcal;
    }
};


// маленький гамбургер із сирною начинкою 
const hamburger = new Hamburger(SIZE_LARGE, STUFFING_POTATO)
// добавка із майонеза
hamburger.addTopping(TOPPING_MAYO);
// питаємо скільки калорий
console.log(`Calories : ${hamburger.calculateCalories()}`);
// скільки коштує
console.log(`Price : ${hamburger.calculatePrice()}`);
// я тут передумав і вирішив додати ще приправу
hamburger.addTopping(TOPPING_SPICE);
// а скільки тепер коштує?
console.log(`Price with sauce: ${hamburger.calculatePrice()}`);
// перевірити, чи великий гамбургер?
console.log(`Is hamburger large: ${hamburger.getSize() === SIZE_LARGE}`);
// не потрібно добавки
hamburger.removeTopping(TOPPING_MAYO);
console.log(`Price : ${hamburger.calculatePrice()}`);
console.log(`Have ${hamburger.getTopping().length} topping/s`);
