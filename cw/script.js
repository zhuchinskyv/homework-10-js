
window.addEventListener('load', () => {
    const calculator = document.querySelector('.calculator'); 
    const keys = calculator.querySelector('.calc_keys'); 
    const display = calculator.querySelector('.calc_display');
    const operatorKeys = keys.querySelectorAll('[data-type="operator"]');

    keys.addEventListener('click', event => {
        if (!event.target.closest('button')) return; 
        const key = event.target;
        const keyValue = key.textContent; 
        const displayValue = display.textContent;
        const { type } = key.dataset; 
        
        const { previousKeyType } = calculator.dataset;
        if (type === 'digit') {
            if (displayValue === '0' || previousKeyType === 'operator') {
                display.textContent = keyValue;
            } else {
                display.textContent = displayValue + keyValue; 
            }
            
        }

        if (type === 'operator') {
            
            operatorKeys.forEach(el => { el.dataset.state = '' });
           
            key.dataset.state = 'selected';
            calculator.dataset.firstDigit = displayValue;
            calculator.dataset.operator = key.dataset.key;
           
        }
        
        if (type === 'equal') {
            const firstDigit = calculator.dataset.firstDigit;
            const operator = calculator.dataset.operator;
            const secondDigit = displayValue;
            display.textContent = calculate(firstDigit, operator, secondDigit); 
        }

        if (type === 'reset') {
            display.textContent = '0';
            delete calculator.dataset.firstDigit;
            delete calculator.dataset.operator;
        }

        if (type === 'plus_minus') {
            display.textContent = displayValue * -1;
        }

        if (type === 'percent') {
            display.textContent = displayValue / 100;
        }
      
        calculator.dataset.previousKeyType = type;
    })

    function calculate(firstDigit, operator, secondDigit) {
        firstDigit = parseFloat(firstDigit);
        secondDigit = parseFloat(secondDigit);
        if (operator === 'plus') return firstDigit + secondDigit;
        if (operator === 'minus') return firstDigit - secondDigit;
        if (operator === 'times') return firstDigit * secondDigit;
        if (operator === 'divide') return firstDigit / secondDigit;
    }

})
